import { TempconvPipe } from './tempconv.pipe';

describe('TempconvPipe', () => {

  let tempPipe = new TempconvPipe();

  it('create an instance', () => {
    expect(tempPipe).toBeTruthy();
  });

  it('should return temperature by default in celsius', () => {
    expect(tempPipe.transform(20)).toBe(20);
  });

  it('should return temperature in kelvin', () => {
    expect(tempPipe.transform(20, 'k')).toBe(293.15);
  });

  it('should return temperature in fahrenheit', () => {
    expect(tempPipe.transform(20, 'f')).toBe(68);
  });
});
