import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempconv'
})
export class TempconvPipe implements PipeTransform {

  transform(temperature: number, format?: string): number {

    let result: number;

    switch (format) {
      case 'k':
        result = temperature + 273.15;
        break;
      case 'f':
        result = temperature * 1.8 + 32;
        break;
      default:
        result = temperature;
        break;
    }

    return result;
  }

}
