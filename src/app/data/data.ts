export const DATA = {

    cities: [
        3873544, // Santiago
        3435910, // Buenos Aires
        3936456, // Lima
        3448439 // Sao Paulo
    ],

    measurements: [
        { id: 1, name: 'Celsius' },
        { id: 2, name: 'Fahrenheit' },
        { id: 3, name: 'Kelvin' }
    ]
};
