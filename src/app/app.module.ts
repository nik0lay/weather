import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

// Routes
import { ROUTES } from './app.routes';

// Services
import { WeatherService } from './services/weather.service';
import { StorageService } from './services/storage.service';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/layout/navbar/navbar.component';
import { LoadingComponent } from './components/layout/loading/loading.component';

// Pipes
import { TempconvPipe } from './pipes/tempconv.pipe';
import { HistoricComponent } from './components/historic/historic.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoadingComponent,
    TempconvPipe,
    HistoricComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(ROUTES, { useHash: true })
  ],
  providers: [
    WeatherService,
    StorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
