import { Injectable } from '@angular/core';

// Models
import { City } from '../models/city';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  /*
  * Function to get data from session storage
  */
  getHistoric() {
    return JSON.parse(sessionStorage.getItem('citiesToStore'));
  }

  /*
  * Function to save historic in session storage
  */
  saveHistoric(citiesList: any[]) {

    let citiesStored = JSON.parse(sessionStorage.getItem('citiesToStore'));

    let citiesToStore: City[] = [];
    let cityToStore: City = new City(null, '', []);
  
    /* 
    * Check if there's already any cities data stored
    * If there isn't any data we store input data
    */
    if (!citiesStored) {
      citiesList.forEach(city => {
        cityToStore.id = city.id;
        cityToStore.name = city.name;
        cityToStore.historic.push(city.main.temp);
  
        citiesToStore.push(cityToStore);
        cityToStore = new City(null, '', []);
      });
      sessionStorage.setItem('citiesToStore', JSON.stringify(citiesToStore));
      
    } else {
      // If there's stored data we have to add new records to existent.
      citiesStored.forEach(cityStored => {
        citiesList.forEach(city => {
          if (city.id === cityStored.id) {
            cityStored.historic.push(city.main.temp);
          }
        });
      });
  
      sessionStorage.removeItem('citiesToStore');
      sessionStorage.setItem('citiesToStore', JSON.stringify(citiesStored));
    }
  
  }
}
