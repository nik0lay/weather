import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { WeatherService } from './weather.service';
import { environment } from 'src/environments/environment';

describe('WeatherService', () => {

  let _weatherService: WeatherService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WeatherService],
      imports: [
        HttpClientTestingModule
      ]
    });

    _weatherService = TestBed.get(WeatherService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should get some data from API', () => {

    const citiesList = [
      {sys: {country: 'CL'}, weather: [{description: 'few clouds', icon: '02d'}], main: {temp: 32.49}, dt: 1549230580, id: 3873544, name: 'Región Metropolitana de Santiago'},
      {sys: {country: 'AR'}, weather: [{description: 'clear sky', icon: '01d'}], main: {temp: 22.99}, dt: 1549230580, id: 3435910, name: 'Buenos Aires'},
      {sys: {country: 'PE'}, weather: [{description: 'scattered clouds', icon: '03d'}], main: {'temp': 27}, dt: 1549230580, id: 3936456, name: 'Lima'},
      {sys: {country: 'BR'}, weather: [{description: 'thunderstorm with rain', icon: '11d'}],  main: {temp: 25.53}, dt: 1549230580, id: 3448439, name: 'Sao Paulo'}
    ];

    const params = {
      id: [],
      APPID: environment.API_KEY,
      units: 'metric'
    };

    _weatherService.getWeatherData([3873544, 3435910, 3936456, 3448439]).subscribe(cities => {
      expect(cities.length).toBe(4);
      expect(cities).toEqual(citiesList);
    });

  });
});
