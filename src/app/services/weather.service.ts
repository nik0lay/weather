import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, flatMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
    private _http: HttpClient
  ) { }

  /*
  * Function to get data from OpenWeatherMap API every 3 minutes
  * We should do it every 10 minutes because at the API doc figures 
  * that their data is getting updated every 10 minutes
  */
  public getWeatherData(cities: number[]) {

    let queryParams = new HttpParams()
      .set('id', cities.toString())
      .set('APPID', environment.API_KEY)
      .set('units', 'metric');

    return timer(0, 180000).pipe(flatMap( () => {
      return this._http.get(environment.API_URL + 'group', {params: queryParams})
        .pipe( map( res => res['list'] ));
    }));
  }
}
