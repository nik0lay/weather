import { TestBed, async } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ROUTES } from './app.routes';

// Components
import { NavbarComponent } from './components/layout/navbar/navbar.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HistoricComponent } from './components/historic/historic.component';
import { LoadingComponent } from './components/layout/loading/loading.component';

// Pipes
import { TempconvPipe } from './pipes/tempconv.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        HistoricComponent,
        NavbarComponent,
        LoadingComponent,
        TempconvPipe
      ],
      imports: [
        FormsModule,
        RouterModule.forRoot(ROUTES)
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
