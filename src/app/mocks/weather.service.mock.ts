import { of } from 'rxjs';

export class WeatherServiceMock {

    private citiesList = [
        {
            sys: {country: 'CL'}, 
            weather: [{description: 'few clouds', icon: '02d'}], 
            main: {temp: 32.49}, 
            dt: 1549404583, 
            id: 3873544, 
            name: 'Región Metropolitana de Santiago'
        },
        {
            sys: {country: 'AR'}, 
            weather: [{description: 'clear sky', icon: '01d'}], 
            main: {temp: 22.99}, 
            dt: 1549404583, 
            id: 3435910, 
            name: 'Buenos Aires'
        },
        {
            sys: {country: 'PE'}, 
            weather: [{description: 'scattered clouds', icon: '03d'}], 
            main: {'temp': 27}, 
            dt: 1549404583, 
            id: 3936456, 
            name: 'Lima'},
        {
            sys: {country: 'BR'}, 
            weather: [{description: 'thunderstorm with rain', icon: '11d'}],  
            main: {temp: 25.53}, 
            dt: 1549404583, 
            id: 3448439, 
            name: 'Sao Paulo'
        }
    ];
    
    public getWeatherData(cities: number[]) {
        
        return of(this.citiesList);
    }
}
