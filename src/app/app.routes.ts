import { Routes } from '@angular/router';

// Components
import { HomeComponent } from './components/home/home.component';
import { HistoricComponent } from './components/historic/historic.component';


export const ROUTES: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'historic',
        component: HistoricComponent
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
    },
    {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'home'
    }
];
