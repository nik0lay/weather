import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

// Services
import { WeatherService } from 'src/app/services/weather.service';
import { StorageService } from 'src/app/services/storage.service';
import { DATA } from 'src/app/data/data';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  // Data from API
  citiesList: any[];
  filteredCities: any[] = [];

  // Error management
  alertMessage: string;
  errorCode: number;

  // Helpers & Preload data
  units = 1;
  imagePath = environment.ICON_URL;
  cities: number[] = DATA.cities;
  measurements: any = DATA.measurements;
  
  
  constructor(
    private _weatherService: WeatherService,
    private _storageService: StorageService
  ) { }

  ngOnInit() {
    this.getWeatherData(this.cities);
  }

  /*
  * Close alert message
  */
  closeError() {
    this.alertMessage = '';
  }

  /*
  * Function to dynamiclly filter by city name
  */
  filterByName(searchString: string) {

    if (!searchString) {
      this.filteredCities = this.citiesList;
    } else {
      this.filteredCities = this.citiesList.filter(city => 
        city.name.toLowerCase().match(searchString.toLowerCase()));
    }
  }
  
  /*
  * API call to get cities data with error management
  */
  private getWeatherData(cityIds: number[]) {

    this._weatherService.getWeatherData(cityIds).subscribe(
      response => {
        // Save returned data as a copy to reset filter
        this.citiesList = response;
        // Copy data because this one is gonna be show to the user
        this.filteredCities = this.citiesList;
        // Store data into session storage, we could also use local storage, to save historic
        this._storageService.saveHistoric(this.citiesList); 
      },
      error => { 
        // Get error message and error code
        this.alertMessage = error.error.message;
        this.errorCode = error.error.cod;
      }
    );
  }

}
