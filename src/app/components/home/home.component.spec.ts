import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

// Components
import { HomeComponent } from './home.component';
import { NavbarComponent } from '../layout/navbar/navbar.component';
import { LoadingComponent } from '../layout/loading/loading.component';

// Services
import { WeatherService } from 'src/app/services/weather.service';
import { WeatherServiceMock } from 'src/app/mocks/weather.service.mock';

// Pipes
import { TempconvPipe } from 'src/app/pipes/tempconv.pipe';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ 
        HomeComponent,
        NavbarComponent,
        LoadingComponent,
        TempconvPipe
       ],
       imports: [
         FormsModule
       ],
       providers: [
         { provide: WeatherService, useClass: WeatherServiceMock }
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the home', () => {
    expect(component).toBeTruthy();
  });

  it('should have four cities', async(() => {
    expect(component.citiesList.length).toEqual(4);
  }));

  it('should have ids of asked cities', async(() => {
    expect(component.cities).toContain(3873544);
    expect(component.cities).toContain(3435910);
    expect(component.cities).toContain(3936456);
    expect(component.cities).toContain(3448439);
  }));

  it('should render city name', async(() => {
    const name = fixture.nativeElement.querySelector('h1');
    expect(name.innerText).toContain('REGIÓN METROPOLITANA DE SANTIAGO (CL)');
  }));

  it('should render city temperature', async(() => {
    const temp = fixture.nativeElement.querySelector('p');
    expect(temp.innerText).not.toBeNull();
  }));

});

