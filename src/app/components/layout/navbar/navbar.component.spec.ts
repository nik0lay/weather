import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Location, APP_BASE_HREF } from '@angular/common';
import { ROUTES } from 'src/app/app.routes';
import { FormsModule } from '@angular/forms';

// Components
import { NavbarComponent } from './navbar.component';
import { HomeComponent } from '../../home/home.component';
import { HistoricComponent } from '../../historic/historic.component';
import { LoadingComponent } from '../loading/loading.component';

// Pipes
import { TempconvPipe } from 'src/app/pipes/tempconv.pipe';

describe('NavbarComponent', () => {

  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        NavbarComponent,
        HomeComponent,
        HistoricComponent,
        LoadingComponent,
        TempconvPipe
      ],
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes(ROUTES)
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    router.initialNavigation();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('navigate to "" redirects you to /home', fakeAsync(() => {
    router.navigate(['']).then(() => {
      expect(location.path()).toBe('/home');
    });
  }));

  it('navigate to "historic" takes you to /historic', fakeAsync(() => {
    router.navigate(['/historic']).then(() => {
      expect(location.path()).toBe('/historic');
    });
  }));

  it('navigate to "home" takes you to /home', fakeAsync(() => {
    router.navigate(['/home']).then(() => {
      expect(location.path()).toBe('/home');
    });
  }));
});
