import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html'
})
export class HistoricComponent implements OnInit {

  historicData: any[] = [];

  constructor(
    private _storageService: StorageService
  ) {
    this.historicData = this._storageService.getHistoric();
  }

  ngOnInit() {
  }

}
